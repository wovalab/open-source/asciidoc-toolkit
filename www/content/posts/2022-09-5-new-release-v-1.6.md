---
title: "New release: v1.6.0"
date: 2022-09-05T09:43:05+02:00
tags: ["release"]
draft: false
---

This version is bringing three new experimental blocs dedicated to data visualization.
With these new blocs you can insert the following chart in your Asciidoc files:


* Line chart


![Line chart](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/line-chart.svg)


* Grouped bar chart


![Grouped bar chart](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/grouped-bar-chart.svg)


* Radar chart


![Radar chart](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/radar.svg)


**NOTE:** We expect to implement more advanced features related to data visualization in the future. Any feedback on this first implementation is more than welcome on our [Zulip chat](https://wovalab-open-source-projects.zulipchat.com/#recent_topics)

**DOCUMENTATION:** You can download the toolkit documentation [here](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/doc/AsciiDoc-for-LabVIEW-v1.6.0.52.pdf)

## Release note

### New:

* Radar chart bloc --> [#56](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/56)
* Bar chart bloc --> [#57](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/57)
* Line chart bloc --> [#58](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/58)

<!--more-->

## Package Download

The package is directly available through the free version of [VI Package Manager](https://vipm.jki.net/get).

## LabVIEW supported version

2014 (32 and 64 bit) and above
